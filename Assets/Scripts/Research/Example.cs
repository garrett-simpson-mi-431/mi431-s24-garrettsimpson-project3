using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    [SerializeField] private EnumDataContainer<TextStyle,TextStyles> textStyles;
    [SerializeField] private int test = 0;


    private void Awake()
    {
        for(int i = 0; i < textStyles.Length; i++)
        {
            Debug.Log(textStyles[i].size);
        }
    }
}

public enum TextStyles
{
    Normal,
    Heading,
    Warning,
    Error,
}

[System.Serializable]
public class TextStyle
{
    public int size;
    public Color color;
}