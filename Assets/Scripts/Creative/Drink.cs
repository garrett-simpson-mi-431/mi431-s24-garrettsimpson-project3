using System.Collections;
using System.Collections.Generic;
using UnityEngine;



    [CreateAssetMenu(fileName = "newDrink", menuName = "Drink")]
    public class Drink : ScriptableObject
    {
        public string drinkName;
        //Stuff like SellPrice, possible effects, etc. will go here later!
    }

