using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newIngredient", menuName = "Ingredient")]
public class Ingredient : ScriptableObject
{
    public int price;
    public Sprite ingredientSprite;
}

