using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AYellowpaper.SerializedCollections;

[CreateAssetMenu(fileName = "newRecipe",menuName = "Recipe")]
public class Recipe : ScriptableObject
{
    //boiling
    [SerializedDictionary(keyName: "Ingredient", valueName: "Amount")]
    public SerializedDictionary<Ingredient, int> recipe;
    public float boilTime;

    //distilling
    public bool isDistillable = false;
    public int distillCycles;
    public float secondsPerCycle;

    //aging
    public bool needsAging = false;
    public int daysToAge;
    //barrel to age in, maybe later

    //product
    public Drink finalProduct;
    public int count = 1;
}

