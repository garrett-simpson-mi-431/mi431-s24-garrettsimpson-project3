using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

[CustomPropertyDrawer(typeof(EnumDataContainer<,>))]
public class EnumDataContainerDrawer : PropertyDrawer
{
    SerializedProperty enumType;
    SerializedProperty content;

    public override VisualElement CreatePropertyGUI(SerializedProperty property)
    {
        
        if (enumType == null)
        {
            enumType = property.FindPropertyRelative("enumType");
        }
        if (content == null)
        {
            content = property.FindPropertyRelative("content");
        }

        Foldout initialFoldout = new();
        initialFoldout.text = property.name;
        //root.Add(initialFoldout);
        initialFoldout.viewDataKey = "initialFoldoutView";

        if (content.arraySize != enumType.enumNames.Length)
        {
            content.arraySize = enumType.enumNames.Length;  
        }

        for(int i = 0; i < content.arraySize; i++)
        {
            SerializedProperty prop = content.GetArrayElementAtIndex(i);
            PropertyField field = new(prop, enumType.enumNames[i]);
            field.BindProperty(prop);
            initialFoldout.Add(field);
        }
        content.serializedObject.ApplyModifiedProperties();

        return initialFoldout;
    }
}
