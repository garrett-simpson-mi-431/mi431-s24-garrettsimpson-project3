using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

[CustomEditor(typeof(Recipe))]
public class RecipeDrawer : Editor
{
    [SerializeField] VisualTreeAsset visualTree;

    private Toggle distillToggle;
    private Toggle ageToggle;

    public override VisualElement CreateInspectorGUI()
    {
        //Applies the UIElements from the UXML file
        VisualElement root = new();
        visualTree.CloneTree(root);

        //Find the toggles and register callbacks
        distillToggle = root.Q<Toggle>("isDistillable");
        distillToggle.RegisterCallback<ChangeEvent<bool>>(OnVisibilityToggle);

        ageToggle = root.Q<Toggle>("needsAging");
        ageToggle.RegisterCallback<ChangeEvent<bool>>(OnVisibilityToggle);

        return root;

    }

    void OnVisibilityToggle(ChangeEvent<bool> evt)
    {
        Toggle toggle = evt.target as Toggle;

        UpdateToggle(toggle);
    }

    void UpdateToggle(Toggle toggle)
    {
        VisualElement panel = toggle.parent.Q<VisualElement>("panel");
        
        if (toggle.value)
        {
            panel.RemoveFromClassList("hidden");
        }
        else
        {
            panel.AddToClassList("hidden");
        }

    }
}
